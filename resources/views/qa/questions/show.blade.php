@extends('qa.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h1>{{$question->title}}</h1>
                    </div>
                    <div class="card-body">
                        <h3>{!!$question->body!!}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
